# n : number of intervals
import operator
import itertools
from numpy import linspace


def len_curve(n):
    import math

    def distance(x1, y1, x2, y2):
        return math.hypot(x2-x1, y2-y1)

    def f(x):
        # return round(x*x, 10)
        return(x*x)

    result = 0
    step = 1/n
    current_x = 0
    next_x = 0
    while current_x + step < 1:
        next_x = current_x+step
        result += distance(current_x, f(current_x), next_x, f(next_x))
        current_x = next_x
    result += distance(next_x, f(next_x), 1, 1)
    return round(result, 9)


print(len_curve(1))
print(len_curve(10))
print(len_curve(100))
print(len_curve(1000))

n = 10
x = [round(x, 9) for x in linspace(0, 1, n+1)]
y = [round(x*x, 9) for x in linspace(0, 1, n+1)]
print(x)
print(y)
