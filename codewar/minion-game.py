# https://www.hackerrank.com/challenges/the-minion-game/problem


def minion_game(string):
    stuart = 0
    kevin = 0
    for i, s in enumerate(string):
        count = len(string)-i
        if s in 'AEIOU':
            kevin += count
        else:
            stuart += count
    if kevin > stuart:
        print(f'Kevin {kevin}')
    elif kevin < stuart:
        print(f'Stuart {stuart}')
    else:
        print('Draw')


if __name__ == '__main__':
    s = 'BANANA'
    minion_game(s)
