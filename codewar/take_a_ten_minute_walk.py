# https://www.codewars.com/kata/54da539698b8a2ad76000228/train/python
def is_valid_walk(walk):
    return (
        (len(walk) == 10) and
        (walk.count('n') == walk.count('s')) and
        (walk.count('w') == walk.count('e')))


print(is_valid_walk(['n', 's', 'n', 's', 'n', 's', 'n', 's', 'n', 's']))
print(is_valid_walk(['w', 'e', 'w', 'e', 'w',
                     'e', 'w', 'e', 'w', 'e', 'w', 'e']))

print(is_valid_walk(['n', 's', 'e', 'w', 'n', 's', 'e', 'w', 'n', 'e']))
