# https://www.codewars.com/kata/551dc350bf4e526099000ae5/train/python


def song_decoder(song):
    return ' '.join(filter(None, song.split('WUB')))


print(song_decoder("AWUBBWUBC"))
print(song_decoder("AWUBWUBWUBBWUBWUBWUBC"))
print(song_decoder("WUBAWUBBWUBCWUB"))
