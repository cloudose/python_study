from random import randint
import time


def countBits(n):
    binary = '{0:b}'.format(n)
    count = 0
    for x in binary:
        if x == '1':
            count += 1
    return count


def countBits2(n):

    binstr = '{0:b}'.format(abs(n))
    return binstr.count('1')


print(countBits(55))
print(countBits2(55))

print(int("100", base=3))
print('-' * 80)

# 1 2 10 11 12 20 21 22 100
# 1 2 3   4  5  6  7  8  9


class Gen:
    def __init__(self):
        self.a = [1, 2, 3, 4]
        self.count = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.count < len(self.a):
            result = self.a[self.count]
            self.count += 1
        else:
            raise StopIteration
        return result


for i in Gen():
    print(i)
    time.sleep(0.2)


for x in iter(lambda: randint(1, 100), 2):
    print(x)
