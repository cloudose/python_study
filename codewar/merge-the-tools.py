# https://www.hackerrank.com/challenges/merge-the-tools/problem?h_r=next-challenge&h_v=zen
def merge_the_tools(string, k):
    chunks = (string[i*k:i*k+k] for i in range(int(len(string)/k)))

    def dedup(s):
        result = []
        for i in s:
            if i not in result:
                result.append(i)
        return ''.join(result)

    for c in chunks:
        print(dedup(c))
    return


if __name__ == '__main__':
    string, k = 'AABCAAADA', 3
    merge_the_tools(string, k)


