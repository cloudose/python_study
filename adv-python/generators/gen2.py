# generators benefits
# 1. process one at a time
# *2. in an orderly way (fix order)
# *3. co-routine (intervene with user code)


class SeqAPI():
    def step1(self):
        print('arbitrary version: me first')

    def step2(self):
        print('arbitrary version: follow by step1')

    def step3(self):
        print('arbitrary version: me last!')


# but you can run in any order, even it might break something
print('arbitrary version')
seq = SeqAPI()
seq.step1()
seq.step3()
seq.step2()


def SeqAPI_gen():
    print('gen version: run me first')
    yield
    print('gen version: follow by step1')
    yield
    print('gen version: off work!')
    yield


print('x'*50)
print("gen version")
steps = SeqAPI_gen()¯
next(steps)
print('after 1st step, rest a bit')
next(steps)
print('hard work done, finally!')
next(steps)
