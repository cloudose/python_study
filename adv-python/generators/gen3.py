from itertools import islice, tee, count
from random import randrange

a = [1, 2, 3]

new_iters = tee(a, 3)

for myi in new_iters:
    for each in myi:
        print(each, end=' ')
    print()


# def compute():
#     return randrange(10)


def infinite():
    count = 0
    while True:
        yield count
        count += 1


result = infinite()
for x in range(10):
    print(next(result), end=' ')
print()

result2 = infinite()


def nwise2(gen, n=2):
    return [next(gen) for _ in range(n)]
    # return zip(range(n), gen)


print('result2', nwise2(result2, 6))
print('result2', nwise2(result2, 6))
print('result2', nwise2(result2, 6))


def nwise(func, n=2):
    return zip(*(islice(func, i, None)
                 for i, g in enumerate(tee(func, n))))


apair = nwise(infinite(), 3)
for _ in range(5):
    print('pair', next(apair))

# print(tee(infinite(), 3))
