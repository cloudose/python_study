# generators benefits
# *1. process one at a time
# 2. in an orderly way (fix order)
# 3. co-routine (intervene with user code)


from time import sleep


def add(x, y):
    return x + y


class Adder():
    def __call__(self, x, y):
        return x+y


add1 = add
add2 = Adder()

print(add1(1, 2))
print(add2(1, 2))

print(type(add1))
print(type(add2))


def compute():
    '''waste time and memory'''
    rv = []
    for i in range(3):
        sleep(0.3)
        rv.append(i)
    return rv


result = compute()
print(result)

# class way


class Compute_iter_version():
    def __init__(self, max):
        self.counter = 0
        self.max = max

    def __iter__(self):
        return self

    def __next__(self):
        sleep(0.3)
        self.counter += 1
        if (self.counter <= self.max):
            return self.counter
        else:
            raise StopIteration()


for x in Compute_iter_version(4):
    print('Compute_iter_version', x)

i = iter(Compute_iter_version(999))
print('Compute_iter_version', next(i))
print('Compute_iter_version', next(i))


# function way (the generator/yield way)
def compute_gen(max):
    for i in range(max):
        sleep(0.3)
        yield(i)


for x in compute_gen(3):
    print('compute_gen', x)
