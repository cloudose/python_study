class Polynomial:
    def __init__(self, *coffes):
        self.coffes = coffes

    def __repr__(self):
        return 'Polynomial(*{!r})'.format(self.coffes)

    def __add__(self, other):
        return Polynomial(*(x+y for x, y in zip(self.coffes, other.coffes)))

    def __len__(self):
        return len(self.coffes)


p1 = Polynomial(1, 2, 3)
p2 = Polynomial(3, 4, 3)

# p1.coeffs=1,2,3   #x^2+2x+3
# p2.coeffs=3,4,3   #3x^2+4x+3

print(p1, p2)
print(p1+p2)

print(len(p1))
