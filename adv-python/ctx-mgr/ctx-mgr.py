from sqlite3 import connect
from contextlib import contextmanager


# Syntax:
# with ctx() as x:
#     pass
#
# x = ctx().__enter__()
# try:
#     pass
# finally:
#     x.__exit__()


class ctxmgr:
    '''class style'''

    def __init__(self, cur):
        self.cur = cur

    def __enter__(self):
        self.cur.execute('create table points(x int, y int)')

    def __exit__(self, exc_type, exc_val, exc_trace):
        self.cur.execute('drop table points')


with connect('test.db') as conn:
    cur = conn.cursor()
    with ctxmgr(cur):
        cur.execute('insert into points (x,y) values (1,2)')
        cur.execute('insert into points (x,y) values (2,4)')
        cur.execute('insert into points (x,y) values (3,5)')
        for row in cur.execute('select x,y from points'):
            print(row)
        for row in cur.execute('select x+y from points'):
            print(row)


@contextmanager
def ctxmgr_gen(cur):
    '''function style'''
    try:
        cur.execute('create table points(x int, y int)')
        yield
    finally:
        cur.execute('drop table points')


with connect('test.db') as conn:
    cur = conn.cursor()
    with ctxmgr_gen(cur):
        cur.execute('insert into points (x,y) values (1,2)')
        cur.execute('insert into points (x,y) values (2,4)')
        cur.execute('insert into points (x,y) values (3,5)')
        for row in cur.execute('select x,y from points'):
            print(row)
        for row in cur.execute('select x+y from points'):
            print(row)
