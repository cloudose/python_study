from inspect import getsource
from time import time


def time_it(func):
    def wrapper(*a, **kw):
        t1 = time()
        result = func(*a, **kw)
        t2 = time()
        print('elapsed', t2-t1)
        return result
    return wrapper


@time_it
def add(x, y):
    '''add two'''
    return x+y


@time_it
def sub(x, y):
    '''sub two'''
    return x-y


print(getsource(add))

print(add(3, 2))
print(add(1, 2))
print(add(2, 2))
print(add('a', 'b'))
print(sub(1, 2))
print(sub(2, 2))


def ntimes(n):
    def run(func):
        def wrapper(*a, **kw):
            for _ in range(n):
                func(*a, **kw)
        return wrapper
    return run


def say_hello(msg):
    print(msg)


five_times = ntimes(5)
helloX5 = five_times(say_hello)
helloX5('hello, my friend')


@ntimes(3)
def say_hello_deco(msg):
    print(msg)


say_hello_deco('me me me')
