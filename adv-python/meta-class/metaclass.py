class BaseMeta(type):
    def __new__(cls, name, bases, body):
        print(cls, name, bases, body)
        return super().__new__(cls, name, bases, body)


class Base(metaclass=BaseMeta):
    def foo(self):
        return self.bar()

    def bar(self):
        raise NotImplementedError


class Derive(Base):
    def bar(self):
        return 'from bar'
