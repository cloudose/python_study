import builtins


class BaseMeta(type):
    def __new__(cls, name, bases, body):
        print('BaseMeta.__new__', cls, name, bases, body)
        return super().__new__(cls, name, bases, body)


class Base(metaclass=BaseMeta):
    def foo(self):
        return 'foo'

    def foo1(self):
        return self.bar1()

    def bar1(self):
        raise NotImplementedError


# keep the old one
# old_bc = builtins.__build_class__


# def my_bc(func, name, base=None, **kw):
#     if base is Base:
#         print('check if bar exists')
#     if base is not None:
#         return old_bc(func, name, base, **kw)
#     return old_bc(func, name, **kw)


# builtins.__build_class__ = my_bc

class Base1():
    def foo(self):
        return 'foo'

    def foo1(self):
        return self.bar1()

    def bar1(self):
        raise NotImplementedError

    def __init_subclass__(cls, *a, **kw):
        print('init_sub', cls, a, kw)
        return super().__init_subclass__(*a, *kw)
