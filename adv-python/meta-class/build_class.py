import builtins


def my_bc(func, name, bases=None, **kw):
    print('build a new class {} based on {}'.format(name, bases))
    if (bases is None):
        return old_bc(func, name, **kw)
    return old_bc(func, name, bases, **kw)


old_bc = builtins.__build_class__
builtins.__build_class__ = my_bc


class Base:
    def foo(self):
        return self.bar()

    def bar(self):
        raise NotImplementedError


class Derive(Base):
    def bar(self):
        return 'from bar'
