from library import Base
from library import Base1

# assert hasattr(Base, 'foo'), 'No foo found in Base'


class Derived(Base):
    def bar(self):
        return self.foo()

    def bar1(self):
        return 'bar1'


d1 = Derived()
print(d1.bar())
print(d1.foo1())


class Derive1(Base1):
    def bar(self):
        return self.foo()

    def bar1(self):
        return 'bar1'


d2 = Derive1()
print(d2.bar())
print(d2.foo1())
