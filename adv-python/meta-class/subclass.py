class Base():
    def __init_subclass__(cls):
        print('i am a subclass', cls)
        return super().__init_subclass__()

    def foo(self):
        return self.bar()

    def bar(self):
        raise NotImplementedError


class AnotherBase():
    pass


class Derive(Base):
    def bar(self):
        return 'from bar'
