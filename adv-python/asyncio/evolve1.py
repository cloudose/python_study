# https://www.youtube.com/watch?v=idLtMISlgy8
# https://emptysqua.re/blog/links-for-how-python-coroutines-work/

import socket
import time
import selectors
from selectors import DefaultSelector
selector = DefaultSelector()
n_tasks = 0


class Future:
    def __init__(self):
        self.cbs = []

    def resolve(self):
        for c in self.cbs:
            c()


class Task:
    def __init__(self, gen):
        self.gen = gen
        self.step()

    def step(self):
        try:
            fut = next(self.gen)
        except StopIteration:
            return
        fut.cbs.append(self.step)


def get(path):
    global n_tasks
    n_tasks += 1
    s = socket.socket()
    s.setblocking(False)
    try:
        s.connect(('velvetblues.com', 80))
    except Exception:
        pass

    # def callback():
        # return connected(s, path)
    f = Future()
    # f.cbs.append(callback)
    selector.register(s.fileno(), selectors.EVENT_WRITE, data=f)
    # selector.select()
    # callback()
    yield f

# def connected(s, path):
    selector.unregister(s.fileno())
    req = 'GET {} HTTP/1.0\r\n\r\n'.format(path)
    s.send(req.encode())
    chunks = []

    def callback():
        return on_read(s, chunks)

    f = Future()
    f.cbs.append(callback)
    selector.register(s.fileno(), selectors.EVENT_READ, data=f)
    # selector.select()
    # callback()


def on_read(s, chunks):
    selector.unregister(s.fileno())
    chunk = s.recv(1024)
    if chunk:
        chunks.append(chunk)

        def callback():
            return on_read(s, chunks)
        f = Future()
        f.cbs.append(callback)
        selector.register(s.fileno(), selectors.EVENT_READ, data=f)
        # selector.select()
        # callback()
    else:
        result = (b''.join(chunks)).decode()
        print(result.split('\n')[0])
        global n_tasks
        n_tasks -= 1
        # return result


start = time.time()
for x in range(3):
    Task(get('/web-development-blog/is-my-website-slow/'))


while n_tasks:
    events = selector.select()
    for key, mask in events:
        fut = key.data
        fut.resolve()

print('{:.2f} secs'.format(time.time()-start))
