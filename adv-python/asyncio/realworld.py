# https://www.youtube.com/watch?v=bckD_GK80oY

import asyncio


async def coro(name):
    print(name)
    await asyncio.sleep(1)


async def main():
    # asyncio.run(coro('1'))
    await asyncio.gather(coro(2), coro(3))

if __name__ == "__main__":
    asyncio.run(main())
