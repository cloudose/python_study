import asyncio


async def find_div(inrange, div_by):
    print('start finding {} divisible by {}'.format(inrange, div_by))
    result = []
    for i in range(inrange):
        if i % div_by == 0:
            result.append(i)
        if i % 1000 == 0:
            await asyncio.sleep(0)

    print('done with {} divisible by {}'.format(inrange, div_by))
    # return result


async def coro(n):
    print(n)
    await asyncio.sleep(1)
    print(n)
    return n

if __name__ == '__main__':
    # t1 = find_div(10000000, 3293)
    # t2 = find_div(1000000, 3293)
    # t3 = find_div(100000, 3293)
    many = coro(1)
    asyncio.run(many)
