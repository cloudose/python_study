import requests

rep = requests.get('https://myfantasy.org')
print(rep.status_code)
print(rep.text)
print(rep.headers)
print(rep.raw)

r = requests.get('https://api.github.com/events')
print(r.json())
