import pygame

pygame.init()
screen = pygame.display.set_mode((800, 600))

# title and icon
pygame.display.set_caption('Space Invaders')
icon = pygame.image.load('games/pygames/ufo.png')
pygame.display.set_icon(icon)

# player
player_img = pygame.image.load('games/pygames/player.png')
player_x = 370
player_xdelta = 0
player_y = 480
player_lastdirect = None
# enemy
enemy_img = pygame.image.load('games/pygames/enemy.png')
enemy_x = 370
enemy_xdelta = 0
enemy_y = 50
enemy_lastdirect = None


def player(x, y):
    screen.blit(player_img, (x, y))


def enemy(x, y):
    screen.blit(enemy_img, (x, y))


running = True
while running:
    # black background  RGB
    screen.fill((0, 0, 0))

    for e in pygame.event.get():
        print(e)
        if e.type == pygame.QUIT:
            running = False
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_LEFT:
                player_xdelta = -4
                player_lastdirect = pygame.K_LEFT
            if e.key == pygame.K_RIGHT:
                player_xdelta = 4
                player_lastdirect = pygame.K_RIGHT
        if e.type == pygame.KEYUP:
            if e.key == pygame.K_RIGHT or e.key == pygame.K_LEFT:
                if e.key == player_lastdirect:
                    player_xdelta = 0

    # update player position
    player_x += player_xdelta

    # boundary
    if player_x <= 0:
        player_x = 0
    if player_x >= 800-64:
        player_x = 800-64

    player(player_x, player_y)
    enemy(enemy_x, enemy_y)

    # update display
    pygame.display.update()
