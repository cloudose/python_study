import pygame

pygame.init()
screen = pygame.display.set_mode((800, 600))

# title and icon
pygame.display.set_caption('Space Invaders')
icon = pygame.image.load('games/pygames/ufo.png')
pygame.display.set_icon(icon)

# player
player_img = pygame.image.load('games/pygames/player.png')
player_xy = (370, 480)


def player():
    screen.blit(player_img, (player_xy))


running = True
while running:
    # black background  RGB
    screen.fill((0, 0, 0))

    for e in pygame.event.get():
        print(e)
        if e.type == pygame.QUIT:
            running = False
    player()

    pygame.display.update()
