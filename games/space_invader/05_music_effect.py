import random
import math

import pygame
from pygame import mixer


pygame.init()
screen = pygame.display.set_mode((800, 600))


# title and icon
pygame.display.set_caption('Space Invaders')
icon = pygame.image.load('games/pygames/ufo.png')
pygame.display.set_icon(icon)
background = pygame.image.load('games/pygames/background.png')

# music
mixer.music.load('games/pygames/background.wav')
# mixer.music.play(-1)

# player
player_img = pygame.image.load('games/pygames/player.png')
player_x = 370
player_xdelta = 0
player_y = 480

# enemy
enemy_img = pygame.image.load('games/pygames/enemy.png')
enemy_x = random.randrange(0, 800-64)
enemy_xdelta = 40
enemy_y = 50
enemy_lastdirect = None

# bullet
bullet_img = pygame.image.load('games/pygames/bullet.png')
bullet_x = 0
bullet_y = 0
bullet_ydelta = -5
bullet_onfire = False

score_value = 0


def display_score():
    font = pygame.font.Font('freesansbold.ttf', 32)
    txt = font.render('Score: {}'.format(score_value), True, (255, 255, 255))
    screen.blit(txt, (0, 0))


def game_over_text():
    font = pygame.font.Font('freesansbold.ttf', 64)
    txt = font.render('GAME OVER!', True, (255, 255, 255))
    screen.blit(txt, (200, 250))


def player(x, y):
    screen.blit(player_img, (x, y))


def enemy(x, y):
    screen.blit(enemy_img, (x, y))


def fire_bullet(x, y):
    global bullet_onfire, bullet_x, bullet_y, mixer
    bullet_onfire = True
    bullet_x = x+16
    bullet_y = y-10
    bullet_sound = mixer.Sound('games/pygames/laser.wav')
    # bullet_sound.play()
    screen.blit(bullet_img, (bullet_x, bullet_y))


def isCollision(enemyX, enemyY, bulletX, bulletY):
    global mixer
    distance = math.hypot(enemyX - bulletX, enemyY - bulletY)
    if distance < 27:
        sound = mixer.Sound('games/pygames/explosion.wav')
        # sound.play()
        return True
    else:
        return False


def isGameOver(enemyX, enemyY, bulletX, bulletY):
    global mixer
    distance = math.hypot(enemyX - bulletX, enemyY - bulletY)
    if distance < 27:
        return True
    else:
        return False


running = True
gameover = False
while running:
    # black background  RGB
    screen.fill((0, 0, 0))
    screen.blit(background, (0, 0))

    for e in pygame.event.get():
        # print(e)
        if e.type == pygame.QUIT:
            running = False
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_LEFT:
                player_xdelta = -4
                player_lastdirect = pygame.K_LEFT
            if e.key == pygame.K_RIGHT:
                player_xdelta = 4
                player_lastdirect = pygame.K_RIGHT
            if e.key == pygame.K_SPACE:
                if not bullet_onfire:
                    fire_bullet(player_x, player_y)
                    print('fire bullet {} {}'.format(player_x, player_y))
                else:
                    pass
        if e.type == pygame.KEYUP:
            if e.key == pygame.K_RIGHT or e.key == pygame.K_LEFT:
                if e.key == player_lastdirect:
                    player_xdelta = 0

    # update player/enemy position
    player_x += player_xdelta
    enemy_x += enemy_xdelta

    # Game over
    if isGameOver(enemy_x, enemy_y, player_x, player_y):
        gameover = True
        enemy_xdelta = 0
    if gameover:
        game_over_text()

    # hit enemy
    if isCollision(enemy_x, enemy_y, bullet_x, bullet_y):
        print('HIT!!!')
        score_value += 1
        print(score_value)
        bullet_onfire = False
        bullet_y = 480
        enemy_xdelta += 0.1
        enemy_x = random.randrange(0, 800-64)

    # boundary
    if player_x <= 0:
        player_x = 0
    if player_x >= 800-64:
        player_x = 800-64
    if enemy_x <= 0:
        enemy_x = 0
        enemy_y += 64
        enemy_xdelta = -enemy_xdelta
    if enemy_x >= 800-64:
        enemy_x = 800-64
        enemy_y += 64
        enemy_xdelta = -enemy_xdelta
    if bullet_y <= 0:
        bullet_onfire = False

    player(player_x, player_y)
    enemy(enemy_x, enemy_y)
    if bullet_onfire:
        bullet_y += bullet_ydelta
        screen.blit(bullet_img, (bullet_x, bullet_y))
    display_score()
    # print('bullet {} {} {}'.format(bullet_onfire, bullet_x, bullet_y))
    # update display
    pygame.display.update()

input()
