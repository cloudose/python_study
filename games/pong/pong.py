'''a pong game'''
import turtle
import random


wn = turtle.Screen()

wn.title('Pong by WZ')
wn.bgcolor('black')
wn.setup(width=800, height=600)
wn.tracer(0)

# paddle A
padA = turtle.Turtle()
padA.speed(0)
padA.shape('square')
padA.color('white')
padA.shapesize(stretch_wid=5, stretch_len=1)
padA.penup()
padA.goto(-350, 0)

# paddle B
padB = turtle.Turtle()
padB.speed(0)
padB.shape('square')
padB.color('white')
padB.shapesize(stretch_wid=5, stretch_len=1)
padB.penup()
padB.goto(350, 0)

# Ball
ball = turtle.Turtle()
ball.speed(0)
ball.shape('circle')
ball.color('white')
ball.penup()
ball.goto(0, 0)
ball.dx = 2
ball.dy = 3

# Pen
pen = turtle.Turtle()
pen.speed(0)
pen.color('white')
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
pen.write('PlayerA:{}    PlayerB:{}'.format(0, 0),
          align='center', font=('Courier', 24, 'normal'))


def pad_move(pad, direction):
    '''pad move'''
    def wrap():
        y = pad.ycor()
        # print('ycord', y)
        # within y range vertical
        if -300+50 <= y and y <= 250:
            if direction is 'up':
                y += 25
            else:
                y -= 25
            pad.sety(y)
            # too much, fix it
            if y > 250:
                pad.sety(250)
            if y < -250:
                pad.sety(-250)
            # print('new position', y)
    return wrap


# kb binding
wn.listen()
wn.onkeypress(pad_move(padA, 'up'), 'w')
wn.onkeypress(pad_move(padA, 'down'), 's')
wn.onkeypress(pad_move(padB, 'up'), 'i')
wn.onkeypress(pad_move(padB, 'down'), 'k')


scoreA = 0
scoreB = 0

while True:
    wn.update()

    ball.setx(ball.xcor()+ball.dx)
    ball.sety(ball.ycor()+ball.dy)

    # left/right boundary
    if abs(ball.xcor()) > 390:
        # left A lose, right B lose
        if ball.xcor() < 0:
            scoreB += 1
            pen.clear()
            pen.write('PlayerA:{}    PlayerB:{}'.format(scoreA, scoreB),
                      align='center', font=('Courier', 24, 'normal'))
        else:
            scoreA += 1
            pen.clear()
            pen.write('PlayerA:{}    PlayerB:{}'.format(scoreA, scoreB),
                      align='center', font=('Courier', 24, 'normal'))

        # reset ball
        ball.goto(0, 0)
        # random direction
        ball.dx *= pow(-1, random.randint(1, 2))
        ball.dy *= pow(-1, random.randint(1, 2))

    # up/down boundary
    if abs(ball.ycor()) > 290:
        ball.dy = -ball.dy

    # collision
    if ball.xcor() > 340 and ball.xcor() < 350 and (abs(ball.ycor() - padB.ycor()) < 50):
        ball.dx *= -1
    if ball.xcor() < -340 and ball.xcor() > -350 and (abs(ball.ycor() - padA.ycor()) < 50):
        ball.dx *= -1
