import datetime
import calendar
import random

import numpy
import matplotlib


def empty():
    pass


def hello():
    # print('hello!')
    return 'hello from function'


print(hello)
print(hello())
for i in range(5):
    print(hello())


def greet(name, msg="long time no seen"):
    """Greet someone with politeness."""
    print(f'{name}, {msg}!')


greet('William')
greet('Alex', 'hello Alexa')
greet('Alex', msg='hello Alexa')


def print_args(*args, **kwargs):
    """Print function args."""
    print('args', args)
    print('kwargs', kwargs)


print_args(1, 3, 2, a=1)

# unpack args
my_args = ['a', 'b', 'c']
my_kwargs = {"name": 'myname', 'age': 30}
print_args(my_args, my_kwargs)  # two position args
print_args(*my_args, **my_kwargs)  # 3 args + 1 kwargs

print(random.choice(list(range(55))))
print(datetime.date.today())

