import datetime
import pytz

print(datetime.datetime.now())
print(datetime.date(2016, 3, 4), datetime.datetime(1983, 7, 12, 12, 45, 0))

today = datetime.date.today()
print('{}th day of the week'.format(today.isoweekday()))


tdel = datetime.timedelta(days=1)
print('today', today)
print('1 day after', today+tdel)
print('1 day before', today-tdel)

bday = datetime.date(2020, 7, 12)
till_bday = bday - today
print(till_bday, 'days to next dob',
      'which is {:,} seconds'.format(till_bday.total_seconds()))

t = datetime.time(3, 4, 40)
print(t)

fulldatetime = datetime.datetime(2020, 2, 7, 12, 17, 0, 0)
print(fulldatetime, fulldatetime.date(), fulldatetime.time())

tdel_1h = datetime.timedelta(hours=1)
print(fulldatetime+tdel_1h)

# pytz
dt = datetime.datetime(1983, 7, 12, 12, 45, 0, tzinfo=pytz.UTC)
print('UTC', dt)

dt = datetime.datetime(1983, 7, 12, 12, 45, 0,
                       tzinfo=pytz.timezone('Asia/Tokyo'))
print('Tokyo', dt)

dt_utcnow = datetime.datetime.now(tz=pytz.UTC)
print('UTC now', dt_utcnow)

dt_pst = dt_utcnow.astimezone(pytz.timezone('America/Vancouver'))
print(dt_pst)

# for tz in pytz.all_timezones:
# print(tz)

# naive -> timezone-aware
dt_local = datetime.datetime.now()
dt_prc = dt_local.astimezone(pytz.timezone('PRC'))
print('Vancouver: {}'.format(dt_local))
print('China: {}'.format(dt_prc))

# time in New York is 9AM, time in Germany?
tz_1 = pytz.timezone('PRC')
dt_0 = datetime.datetime(1983, 7, 12, 12, 00)
print('dt_0', dt_0)
dt_1 = tz_1.localize(dt_0)
print('dt_1', dt_1)
tz_2 = pytz.timezone('GMT')
print('dt2', dt_1.astimezone(tz_2))

