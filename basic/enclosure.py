def counter():
    count = 0

    def increase():
        nonlocal count
        count += 1
        return count
    return increase


increase = counter()

for _ in range(10):
    print(increase())


def create_logger(prefix):
    def log(msg):
        print(f'{prefix}: {msg}')
    return log


log_warn = create_logger('WARN')
log_err = create_logger('ERR')

log_warn('this is a warning')
log_err('something went WRONG!')


def log(prefix):
    def fancy_log(msg):
        print(f'{prefix}----{msg}')
    return fancy_log


prefix_log = log('a prefix')
prefix_log(123)
