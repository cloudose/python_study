class Employee:

    def __init__(self, first_name, last_name, pay):
        '''constructor'''
        self.first_name = first_name
        self.last_name = last_name
        self.pay = pay
        # self.email = '{}.{}@corp.com'.format(first_name, last_name)

    @property
    def fullname(self):
        '''full name'''
        return '{} {}'.format(self.first_name, self.last_name)

    @fullname.setter
    def fullname(self, name):
        self.first_name, self.last_name = name.split(' ')

    @fullname.deleter
    def fullname(self):
        self.first_name = None
        self.last_name = None

    @property
    def email(self):
        '''email'''
        return '{}.{}@corp.com'.format(self.first_name, self.last_name)


emp1 = Employee('F', 'L', 1024)
print(emp1.fullname)
print(emp1.email)

emp1.first_name = 'changed'
print(emp1.fullname)
print(emp1.email)

emp1.fullname = 'New Name'
print(emp1.fullname)
print(emp1.email)

del(emp1.fullname)
print(emp1.fullname)
print(emp1.email)
