# print msg
print("hello world")
print(10)

# variable
a = 10
print(a)
check_signal = 100

# quotes
quotes = 'this var includes an " in the middle'
pre_formatted_str = '''fdjsakl
fdsa
fds
afs
af
sa
fdsa
'''
print(quotes)
print(len(pre_formatted_str))

# slices
print(quotes[0:3])
print(quotes[1:3])
print(quotes[:3])
print(quotes[6:])

# str functions
print(quotes.capitalize())
print(quotes.count("a"))
print(quotes.find("a"))

new_quote = quotes.replace('includes', 'has')
print(new_quote)

# concatenate strs
msg1 = 'hello'
msg2 = 'world'
print(msg1+' '+msg2)

# format string
whole_msg = '{}, {}!'.format(msg1, msg2)
print(whole_msg)

whole_msg1 = f'{msg1}, {msg2}!'
print(whole_msg1)
print(dir(msg1))

# help / man
# help(str)
# print(help(str.lower))

# numbers
num = 3
print(type(num))

float_num = 3.3
print(type(float_num))

# maths
print(1+2)
print(1-2)
print(1*3)
print(5/2)  # 2.5
print(5//2)  # 2
print(5 % 2)  # 1
print(3**2)  # 9

num += 5  # num = num+5

abs(-5)
print(round(-5.2032), round(-5.325812, 3))
# -5  -5.326

print(1 == 2, 1 != 2, 1 > 2, 1 < 2)
# False True False True

# str <=> num
str_num1 = "11"
str_num2 = "22"
print(str_num1+str_num2)  # 1122
num1 = int(str_num1)
num2 = int(str_num2)
print(num1+num2)  # 33
# str_num1 + num1  ERROR!


# lists, tuples, sets
courses = ['Maths', 'Physics', "History", "Japanese"]
print(courses[0], courses[0:2], courses[-2:])
# Maths ['Maths','Physics'] ['History','Japanese']
# courses[4]   ERROR

# append / extend / remove / pop
courses.append('French')
print(courses)
courses.insert(1, 'Art')
print(courses)
summer_courses = ['Chinese', 'Python']

courses.extend(summer_courses)  # flatten
print(courses)
courses.append(summer_courses)  # as a whole
print(courses)

courses.remove('History')
print(courses)

pop_last = courses.pop()
pop_first = courses.pop(0)
print(pop_last, pop_first, courses)

# sort
courses.sort(reverse=True)  # sort in place
print(courses)

sorted_courses = sorted(courses)  # return new list
print(sorted_courses)

# in / index
print(courses.index('Japanese'), ('Latin' in courses))  # 2  False

# join / split
str_courses = ' - '.join(courses)
print(str_courses)
split_courses = str_courses.split(' - ')
print(split_courses)

# lists vs tuples
list1 = ['a', 'b', 'c', 'd']
list2 = list1
print(list1, list2)
list1[0] = 'A'
print(list1, list2)  # list2[0] = A now

tuple1 = ('a', 'b', 'c', 'd')
tuple2 = tuple1
print(tuple1, tuple2)
# tuple1[0] = 'A'  # ERROR, no assignment allowed
# print(tuple1, tuple2)

# sets  w/o order
students = {'John', 'Jack', 'Jason', 'John'}
print(students, 'Julie' in students)
# 2nd John missing!   and Julie not in the class.

# interset
students2 = {'William', 'Lisa', 'Louise', 'John'}
print('intersects ', students.intersection(students2))
print('difference ', students.difference(students2))
print('difference ', students2.difference(students))
print('union ', students.union(students2))

# new list/tuple/set
empty_list = []
empty_list2 = list()
empty_tuple = ()
empty_tuple2 = tuple()
# empty_set = {}  #WRONG, it is a dict
empty_set = set()

# loop
for c in courses:
    print(c)
for i, c in enumerate(courses):
    print(i, c)
for i, c in enumerate(courses, start=1):
    print(i, c)

# dict
studentA = {'name': 'John', 'age': 35, 'courses': ['Maths', 'CompSci']}
print(studentA)
print(studentA['name'])

objA = {1: 'John'}
print(objA[1])
# print(objA['NonExistKey'])  #ERROR

print(studentA.get('name'))
print(studentA.get('NonExistKey'))  # None
print(studentA.get('NonExistKey', 'TryDigMore'))  # default is TryDigMore

# update value
studentA['phone'] = '555-555-555'
print(studentA.get('phone'))

studentA['age'] += 1
print(studentA['age'])

studentA.update({'name': 'Jane', 'age': 30, 'phone': '666-777'})
print(studentA)

# delete
del studentA['courses']
print('no more courses', studentA)

age = studentA.pop('age')
print('never grow old?', age, studentA)

# keys/items
print(studentA.keys())
print(studentA.items())
for index, item in studentA.items():
    print(index, item)
for index, item in enumerate(studentA.keys()):
    print('enum keys ', index, item)


# bool, if
if True:
    print('true!')

if studentA.get('name') == 'Jane':
    print('Hi {}'.format('Jane'))

studentA['age'] = 15
if studentA.get('age') < 18:
    print('too young!')
else:
    print("Let's go to a bar and have fun!")

studentA['age'] = 30
if studentA.get('age') < 18:
    print('too young!')
else:
    print("Let's go to a bar and have fun!")

# if/elif/else
if False:
    print(0)
elif True:
    print(111)
else:
    print(2)

# is / ==
a = [1, 2, 3]
b = [1, 2, 3]
a_clone = a
print('a==b', a == b)
print('a is b', a is b)
print('a is a_clone', a is a_clone)  # underlying compare using id
print(id(a), id(b), id(a_clone))

# loops
nums = [1, 2, 3, 4, 5, 6, 7, 8, 9]
for num in nums:
    if num == 8:
        print('no more! too much!!')
        break
    if num % 2 == 0:
        print('even num!', num)
        continue
    print(num, num % 2)

for num in [1, 2, 3]:
    for c in 'abc':
        print(num, c)

print(range(5))
for n in range(5):
    print(n)

x = 0
while x < 5:
    if x == 3:
        print('skip 3!')
        x += 1  # or infinite loop!
        continue
    print('x count', x)
    x += 1


a_list = list(range(10))
print(a_list, a_list[::-1])

a_str = 'abcdefghijklmn'
print(a_str[3:-2])
print(a_str[3:-2][1:3])


list3 = [2, 3, 7, 5, 4, 8, 3, 4, 3, 1, 9]
print('sorted', sorted(list3))

list4 = list(list3)
list4.sort()
print('list3', list3)
print('list4', list4)

s_tuple = sorted(tuple1)
print('sorted tuple', type(s_tuple), s_tuple)

dict1 = {'name': 'Bill', 'job': 'hmmm', 'age': 36, 'os': 'Mac'}
print('sorted (keys)', sorted(dict1))

list5 = [{key: dict1[key]} for key in sorted(dict1)]
print('combine', list5)

list6 = [1, 2, 3, -4, -5, -6]
print(sorted(list6))
print(sorted(list6, key=(lambda x: abs(x))))
print(sorted(list6, key=abs))
print(type(lambda x: x))


sentence = 'I am {name} and I am {age} years old. I use {os} operating system!'.format(
    name='ww', age='33', os='mac')
print(sentence)

dict1 = {'name': 'Bill', 'job': 'hmmm', 'age': 36, 'os': 'Mac'}
sentence = 'I am {name} and I am {age} years old. I use {os} operating system!'.format(
    **dict1)
print(sentence)


print('PI is {:09}'.format(3.1415))


a = [1, 2, 3, 4, 5]
a[0:3] = [x + 5 for x in a[0:3]]
print(a)
