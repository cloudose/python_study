class Employee:
    raise_percent = 5
    count = 0

    def __init__(self, first_name, last_name, pay):
        '''constructor'''
        self.first_name = first_name
        self.last_name = last_name
        self.pay = pay
        self.email = '{}.{}@corp.com'.format(first_name, last_name)
        Employee.count += 1

    def bio(self):
        '''a short bio'''
        return 'My name is {}, and my email is {}. I earned ${} last year!'.format(self.fullname(), self.email, self.pay)

    def fullname(self):
        '''full name'''
        return '{} {}'.format(self.first_name, self.last_name)

    def salary_raise(self):
        '''more income annually'''
        self.pay = self.pay+int(self.pay*self.raise_percent/100)


emp1 = Employee('Q', 'Z', 999999)
emp2 = Employee('L', 'L', 999999)
emp3 = Employee('A', 'A', 0)

print(emp1.bio())
emp1.salary_raise()
print(emp1.bio())

print(dir(Employee))
print(dir(emp1))
print('Employee dict', Employee.__dict__)
print('emp1 dict', emp1.__dict__)

print(Employee.count)
