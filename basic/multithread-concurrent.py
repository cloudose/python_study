import concurrent.futures
import time


def do_sth(argv):
    print(f'sleep {argv} second(s)')
    time.sleep(argv)
    return(f'woke up after {argv} second(s)')


start = time.perf_counter()


# with concurrent.futures.ThreadPoolExecutor() as executor:
#     results = [executor.submit(do_sth, 1.5) for _ in range(10)]
#     for f in concurrent.futures.as_completed(results):
#         print(f.result())

with concurrent.futures.ThreadPoolExecutor() as executor:
    results = executor.map(do_sth, range(5, 1, -1))
    for r in results:
        print(r)


end = time.perf_counter()
print(end-start)
