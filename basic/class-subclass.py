import datetime


class Employee:
    raise_percent = 5
    count = 0   # employee count

    def __init__(self, first_name, last_name, pay):
        '''constructor'''
        self.first_name = first_name
        self.last_name = last_name
        self.pay = pay
        self.email = '{}.{}@corp.com'.format(first_name, last_name)
        Employee.count += 1

    def bio(self):
        '''a short bio'''
        return 'My name is {}, and my email is {}. I earned ${} last year!'.format(self.fullname(), self.email, self.pay)

    def fullname(self):
        '''full name'''
        return '{} {}'.format(self.first_name, self.last_name)

    def salary_raise(self):
        '''more income annually'''
        self.pay = self.pay+int(self.pay*self.raise_percent/100)

    @classmethod
    def set_raise_amt(cls, amount):
        '''set Employee raise percentage'''
        cls.raise_percent = amount

    @classmethod
    def from_string(cls, str):
        '''build from string'''
        first, last, pay = str.split('-')
        return cls(first, last, pay)


class Developer(Employee):
    raise_percent = 10

    def __init__(self, first_name, last_name, pay, lang):
        '''constructor'''
        super().__init__(first_name, last_name, pay)
        # Employee.__init__(self,first,last,pay)
        self.lang = lang

    def bio(self):
        return '{}. I can program in these languages:{}'.format(super().bio(), self.lang)


class Manager(Employee):
    def __init__(self, first_name, last_name, pay, employees=None):
        super().__init__(first_name, last_name, pay)
        if employees is None:
            self.employees = []
        else:
            self.employees = employees

    def add_employee(self, employee):
        if employee not in self.employees:
            self.employees.append(employee)

    def team(self):
        return tuple(map(lambda x: x.fullname(), self.employees))


dev1 = Developer('Q', 'Z', 100000, 'Python, JavaScript, Perl, Shell')
print('bio:', dev1.bio())
print('type:', type(dev1))
# print(help(Developer))
# print(dev1.bio())
# dev1.salary_raise()
# print(dev1.bio())

dev2 = Developer('A', 'A', 100000, 'C#, Java, LISP')

manager = Manager('Boss', 'X', 123456, [dev1])
print(manager.bio())
# print(manager.team())
manager.add_employee(dev2)
print('My team:', manager.team())

print('manager or employee?', isinstance(
    manager, (Manager, Employee)))  # manager or employee?
print('developer?', isinstance(manager, Developer))  # developer?

print(
    issubclass(Developer, Employee),
    issubclass(Manager, Employee),
    issubclass(Manager, Developer))

