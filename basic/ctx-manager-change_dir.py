from contextlib import contextmanager
import os

cwd = os.getcwd()
os.chdir('__pycache__')
print(os.listdir())
os.chdir(cwd)

cwd = os.getcwd()
os.chdir('.vscode')
print(os.listdir())
os.chdir(cwd)


@contextmanager
def change_dir(destination):
    try:
        current = os.getcwd()
        os.chdir(destination)
        yield
    finally:
        os.chdir(current)


with change_dir('__pycache__'):
    print(os.listdir())

with change_dir('.vscode'):
    print(os.listdir())
