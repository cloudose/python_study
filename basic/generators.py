def square(nums):
    result = []
    for i in nums:
        result.append(i*i)
    return result


nums = [1, 2, 3, 4, 5]
print(square(nums))


def square_generator(nums):
    for i in nums:
        yield i*i


g = square_generator(nums)
print(g)
print(next(g))
print('rest in for')

for i in g:
    print(i)

# print(next(g))  # ERROR run out of nums

list_comprehension = [x*x for x in nums]
print('compr', list_comprehension)

list_generator = (x*x for x in nums)
print('gen', list_generator)
print(list(list_generator))


def gen_fibo():
    a, b = 0, 1
    while True:
        a, b = b, a+b
        yield a


g_fibo = gen_fibo()
for i in range(1, 100):
    print(next(g_fibo), end=' ')

print(type(range(1, 3)))
