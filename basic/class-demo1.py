# OOP
class Employee:
    pass


print(Employee)

emp1 = Employee()
emp1.first = 'Will'
emp1.last = 'Zhou'

emp2 = Employee()
emp2.email = 'emp2@corp.com'

print(emp1, emp2, 'equal?', emp1 == emp2)
