fd = open('intro.py', 'r')
print(fd.name, fd.mode)
fd.close()

with open('intro.py', 'r') as fd:
    print(fd.name, fd.mode)
    # result = fd.readlines()
    # print(result)
    for l in fd:
        l1 = l.rstrip()
        # print(l1)
print('file is closed:', fd.closed)


with open('intro.py', 'r') as fd:
    chunk_size = 1000
    result = fd.read(chunk_size)
    while len(result) > 0:
        # print(result, end='*')
        print(fd.tell())
        result = fd.read(chunk_size)

with open('write_test.txt', 'w') as fd:
    fd.write('1234567890')
    fd.seek(0)
    fd.write('abcde')  # abcde67890
with open('write_test.txt', 'r') as fd:
    print(fd.read())

