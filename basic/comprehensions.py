nums = list(range(10))

my_list_1 = [n for n in nums]
print(my_list_1)

my_list_2 = [n**2 for n in nums]
print(my_list_2)

my_list_3 = map(lambda n: n*n, nums)
print(list(my_list_3), (my_list_3))

my_list_4 = list(filter(lambda n: n % 2 == 0, nums))
print(my_list_4)

letters = 'abcd'
nums2 = '0123'
rome = ['IV', 'V', 'VI']

# matrix
my_list_6 = [(l, n, r) for l in letters for n in nums for r in rome]
print('list 6', my_list_6)

# zip
my_list_5 = list(zip(letters, nums))
print('list 5', my_list_5)

heros = ['h1', 'h2', 'h3', 'h4']
heros_names = ['name1', 'name2', 'name3', 'name4']
hero_match = {name: hero for (name, hero) in zip(heros_names, heros)}
print(hero_match)

# generators!!!
my_list_7 = (n for n in nums)
print('generator', type(my_list_7), list(my_list_7))

# no gen, but tuples
gen = (1, 2, 3, 4)
print(type(gen))

