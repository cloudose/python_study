import os

from contextlib import contextmanager

class Read_File():
    def __init__(self, fname, mode):
        self.fname = fname
        self.mode = mode

    def __enter__(self):
        self.fh = open(self.fname, self.mode)
        return self.fh

    def __exit__(self, exc_type, exc_val, traceback):
        self.fh.close()


# open myself and print it out
print(os.path.basename(__file__))
with Read_File(__file__, 'r') as fh:
    print(fh.read())
print('file is closed?', fh.closed)
