import time
from datetime import datetime as dt
from functools import wraps


def time_it(orig_func):
    @wraps(orig_func)
    def time_wrapper(*args, **kwargs):
        t1 = time.time()
        print('TIME:function start executing...')
        result = orig_func(*args, **kwargs)
        print('TIME:function execution ended...')
        duration = time.time()-t1
        print(
            f'TIME:function {orig_func.__name__} execution took {duration} seconds')
        return result
    return time_wrapper


def log_it(orig_func):
    @wraps(orig_func)
    def log_wrapper(*args, **kwargs):
        print('INFO: {} {} {} {}'.format(
            dt.now(), orig_func.__name__, *args, list(**kwargs)))
        return orig_func(*args, **kwargs)
    return log_wrapper


@time_it
@log_it
def sleeps(seconds):
    print('sleep {} seconds'.format(seconds))
    time.sleep(seconds)
    print('woke up!')


sleeps(2)
