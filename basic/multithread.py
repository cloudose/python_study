import time
import threading


def do_sth():
    print('sleep 1s')
    time.sleep(1)
    print('woke up')


start = time.perf_counter()

threads = []
for _ in range(10):
    t = threading.Thread(target=do_sth)
    t.start()
    threads.append(t)

for t in threads:
    t.join()

end = time.perf_counter()
print(end-start)
