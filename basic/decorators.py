def decorator_func(orig_func):
    '''decorator in form of function'''
    def wrapper_function():
        print('in the wrapper!')
        return orig_func()
    return wrapper_function


def display():
    print('display something')


decorated_display = decorator_func(display)
decorated_display()
print(type(decorated_display), decorated_display.__name__)


class decorator_class(object):
    '''decorator in form of class'''

    def __init__(self, orig_func):
        self.orig_func = orig_func

    def __call__(self, *args, **kwargs):
        print('this executes first whenever the function is called')
        self.orig_func(*args, **kwargs)


@decorator_class
def display2(msg):
    print('display something too', msg)


display2('ahaa')
