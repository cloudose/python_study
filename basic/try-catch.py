try:
    f = open('non-exist-file')
except FileNotFoundError as e:
    print('attention! file not found.', e)
except Exception as e:
    print('attention!', e)
else:
    print(f.read())
    f.close()
finally:
    raise Exception()
