from class_employee import Employee
import unittest
from unittest.mock import patch


class test_Employee(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print('setup up class')
        '''say populate a demo DB'''
        pass

    @classmethod
    def tearDownClass(cls):
        print('tear down class')
        pass

    def setUp(self):
        '''run before every test'''
        print('setUp for test')
        self.emp1 = Employee('f', 'l', 1000)
        pass

    def tearDown(self):
        '''run after every test'''
        print('tearDown after test\n')
        pass

    # test的顺序是不保证的!!!

    def test_fullname(self):
        print('fullname')
        self.assertEqual(self.emp1.fullname, 'f l')

    def test_email(self):
        '''correct email'''
        print('email')
        self.assertEqual(self.emp1.email, 'f.l@corp.com')
        self.emp1.first = 'f1'
        self.assertNotEqual(self.emp1.email, 'f.l@corp.com')
        self.assertEqual(self.emp1.email, 'f1.l@corp.com')

    def test_raise(self):
        '''test raise func'''
        print('test raise')
        self.emp1.apply_raise()
        self.assertEqual(1050, self.emp1.pay)
        self.assertNotEqual(1051, self.emp1.pay)

    def test_schedule(self):
        print('test schedule')
        with patch('class_employee.requests.get') as mocked_get:
            mocked_get.return_value.ok = True
            mocked_get.return_value.text = 'Success'
            schedule = self.emp1.monthly_schedule('May')
            mocked_get.assert_called_with('http://xxx.com/l/May')
            self.assertEqual(schedule, 'Success')

            mocked_get.return_value.ok = False
            schedule = self.emp1.monthly_schedule('June')
            mocked_get.assert_called_with('http://xxx.com/l/June')
            self.assertEqual(schedule, 'BAD request!')


if __name__ == "__main__":
    unittest.main()
