class Employee:
    def __init__(self, first_name, last_name, pay):
        '''constructor'''
        self.first_name = first_name
        self.last_name = last_name
        self.pay = pay
        self.email = '{}.{}@corp.com'.format(first_name, last_name)

    def bio(self):
        return 'My name is {}, and my email is {}. I earned ${} last year!'.format(self.fullname(), self.email, self.pay)

    def fullname(self):
        return '{} {}'.format(self.first_name, self.last_name)


emp1 = Employee('Q', 'Z', '999999')
emp2 = Employee('L', 'L', '999999')

print(emp1.bio())

