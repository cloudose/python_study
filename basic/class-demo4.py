import datetime


class Employee:
    raise_percent = 5
    count = 0   # employee count

    def __init__(self, first_name, last_name, pay):
        '''constructor'''
        self.first_name = first_name
        self.last_name = last_name
        self.pay = pay
        self.email = '{}.{}@corp.com'.format(first_name, last_name)
        Employee.count += 1

    def bio(self):
        '''a short bio'''
        return 'My name is {}, and my email is {}. I earned ${} last year!'.format(self.fullname(), self.email, self.pay)

    def fullname(self):
        '''full name'''
        return '{} {}'.format(self.first_name, self.last_name)

    def salary_raise(self):
        '''more income annually'''
        self.pay = self.pay+int(self.pay*self.raise_percent/100)

    @classmethod
    def set_raise_amt(cls, amount):
        '''set Employee raise percentage'''
        cls.raise_percent = amount

    @classmethod
    def from_string(cls, str):
        '''build from string'''
        first, last, pay = str.split('-')
        return cls(first, last, pay)

    @staticmethod
    def isworkday(day):
        '''really has nothing to do with this class'''
        if day.weekday() >= 6:
            return False
        else:
            return True


emp1 = Employee('Q', 'Z', 1000000)
emp2 = Employee('L', 'L', 1000000)
emp3 = Employee('A', 'A', 0)

print(emp1.bio())
emp1.salary_raise()
print(emp1.bio())

Employee.set_raise_amt(10)
print(emp2.bio())
emp2.salary_raise()
print(emp2.bio())

print('is workday?', Employee.isworkday(datetime.datetime.today()))


print(dir(Employee))
print(dir(emp1))
print('Employee dict', Employee.__dict__)
print('emp1 dict', emp1.__dict__)

print(Employee.count)


emp4 = Employee.from_string('A-B-2930')
print(emp4.bio())

print(Employee.count)
