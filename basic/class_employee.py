import requests


class Employee:
    '''set up for unittest-study.py, do NOT change'''
    raise_amount = 1.05

    def __init__(self, first, last, pay):
        '''constructor'''
        self.first = first
        self.last = last
        self.pay = pay

    @property
    def fullname(self):
        '''full name'''
        return '{} {}'.format(self.first, self.last)

    @property
    def email(self):
        '''email'''
        return '{}.{}@corp.com'.format(self.first, self.last)

    def apply_raise(self):
        self.pay = int(self.pay*self.raise_amount)

    def monthly_schedule(self, month):
        res = requests.get(f'http://xxx.com/{self.last}/{month}')
        if res.ok:
            return res.text
        else:
            return "BAD request!"
