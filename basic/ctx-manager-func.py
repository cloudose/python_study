import os

from contextlib import contextmanager

# class Read_File():
#     def __init__(self, fname, mode):
#         self.fname = fname
#         self.mode = mode

#     def __enter__(self):
#         self.fh = open(self.fname, self.mode)
#         return self.fh

#     def __exit__(self, exc_type, exc_val, traceback):
#         self.fh.close()


@contextmanager
def open_file(fname, mode):
    try:
        fh = open(fname, mode)
        yield fh
    finally:
        fh.close()


# open myself and print it out
print(os.path.basename(__file__))
with open_file(__file__, 'r') as fh:
    print(fh.readline())
    print(fh.readline())
    print(fh.readline())
print('file is closed?', fh.closed)
