import sys
from my_module import find_index as findex, mod_str
from my_module import find_index, mod_str
import my_module as mm
import my_module

courses = ['Chinese', 'Maths', 'English']

print(my_module.find_index(courses, 'Maths'))
print(mm.find_index(courses, 'Maths'))
print(find_index(courses, 'Maths'))
print(findex(courses, 'Maths'))
print(mod_str)

print('Load modules from:', sys.path)
# sys.path or PYTHONPATH(only appends)
# e.g.
#   export PYTHONPATH='/home/xxx/python_mod'
