import requests
import time
import sqlite3
import json


def read_from_sqlfile():
    with sqlite3.connect('homebrew.db') as conn:
        conn.row_factory = sqlite3.Row
        conn.execute(
            'CREATE TABLE IF NOT EXISTS homebrewstats(pkg_name PRIMARY KEY, desc, ior30, ior90, ior365)')
        c = conn.execute('SELECT * FROM sqlite_master')
        r = c.fetchone()
        # print(type(r), r.keys())
        for r in c:
            print(r.keys(), tuple(r))


def get_pkgs_list():
    url = 'https://formulae.brew.sh/api/formula.json'
    try:
        res = requests.get(url)
    except Exception as e:
        print(e)
    else:
        jsons = res.json()
        return jsons


def get_pkg_info(pkg):
    url = f'https://formulae.brew.sh/api/formula/{pkg}.json'
    try:
        req = requests.get(url)
        # print('loaded in {} seconds'.format(req.elapsed.total_seconds()))
    except Exception as e:
        print(e)
    else:
        return req.json(), req.elapsed.total_seconds()


def save_to_json():
    results = []
    print('getting list...')
    all_pkgs = get_pkgs_list()
    for pkg in all_pkgs:
        name, desc = pkg['name'], pkg['desc']
        if pkg['name'] == 'annie':
            break
        info, elapsed = get_pkg_info(name)
        print(name, desc, elapsed, 'seconds')
        time.sleep(elapsed+0.3)
        item = {'name': name,
                'desc': desc,
                'analytics': {
                    '30d': info['analytics']['install_on_request']['30d'][name],
                    '90d': info['analytics']['install_on_request']['90d'][name],
                    '365d': info['analytics']['install_on_request']['365d'][name]}
                }
        results.append(item)
    try:
        with open('homebrew_stats.json', 'w') as fh:
            json.dump(results, fh, indent=4)
    except Exception as e:
        print('cannot write to json file', e)


if __name__ == "__main__":
    # save_to_json()
    with open('homebrew_stats.json', 'r') as fh:
        j = json.load(fh)
        # print(json.dumps(j, indent=4))

    def sort_days(days="30d"):
        def wrap(x):
            return x['analytics'][days]
        return wrap

    j.sort(key=sort_days("365d"))
    print(json.dumps(j[-3:], indent=4))

    pass
