import datetime


class Employee:
    raise_percent = 5
    count = 0   # employee count

    def __init__(self, first_name, last_name, pay):
        '''constructor'''
        self.first_name = first_name
        self.last_name = last_name
        self.pay = pay
        self.email = '{}.{}@corp.com'.format(first_name, last_name)
        Employee.count += 1

    def bio(self):
        '''a short bio'''
        return 'My name is {}, and my email is {}. I earned ${} last year!'.format(self.fullname(), self.email, self.pay)

    def fullname(self):
        '''full name'''
        return '{} {}'.format(self.first_name, self.last_name)

    def salary_raise(self):
        '''more income annually'''
        self.pay = self.pay+int(self.pay*self.raise_percent/100)

    def __repr__(self):
        return "Employee('{}', '{}', {})".format(self.first_name, self.last_name, self.pay)

    def __str__(self):
        return self.bio()

    def __add__(self, other):
        return self.pay+other.pay

    @classmethod
    def set_raise_amt(cls, amount):
        '''set Employee raise percentage'''
        cls.raise_percent = amount

    @classmethod
    def from_string(cls, str):
        '''build from string'''
        first, last, pay = str.split('-')
        return cls(first, last, pay)


emp1 = Employee('F', 'L', 1024)
print(emp1)
print(repr(emp1))
print(str(emp1))
emp2 = Employee('F', 'L', 1024)

print(emp1+emp2)

print('1234'.__len__())
