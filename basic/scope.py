'''
LEGB
Local, Enclosing, Global, Built-in
'''

x = 'global x'


def test():
    x = 'local x'
    y = 'local y'

    print('x', x)
    print('y', y)


def test1():
    global x  # x is global instead of local
    global z
    x = 'global x again!'
    y = 'local y'
    z = 'global from test1'

    print('x', x)
    print('y', y)


test()
print('x', x)


test1()
print('x', x)
print('z', z)


def outer():
    x = 'outer x'
    print('outer x', x)

    def inner1():
        # nonlocal x
        x = 'inner1 x'

        print('inner1 x', x)

        def inner2():
            nonlocal x
            # x = 'inner2 x'
            print('inner2 x', x)

        inner2()

    inner1()


outer()
