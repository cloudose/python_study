from itertools import accumulate
import operator

data = [1, 2, 3, 4, 5]

print(list(accumulate(data, operator.add)))
print(list(accumulate(data, operator.mul)))
