from itertools import cycle

counter = cycle([1, 2, 3])
for _ in range(9):
    print(next(counter), end=' ')
print()

counter1 = cycle(('ON', 'OFF'))
for _ in range(9):
    print(next(counter1), end=' ')
print()
