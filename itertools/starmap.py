from itertools import starmap

data = [(1, 2), (2, 3), (3, 4), (4, 5)]

print(list(starmap(pow, data)))


x1 = [pow(x[0], x[1]) for x in data]
print(x1)
