'''chain iterables together'''
from itertools import chain

# these are generators and can't be 'add'ed together like gen1+gen2+gen3
gen1 = (x for x in [1, 2, 3])
gen2 = (x for x in ['a', 'b', 'c'])
gen3 = (x for x in (True, False, None))

print(list(chain(gen1, gen2, gen3)))
