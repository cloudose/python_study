from itertools import dropwhile, takewhile

data = [1, 2, 10, 20, 30, 40, 50, 60]


def gtx(x):
    if x <= 30:
        return True
    else:
        return False


print(list(dropwhile(gtx, data)))
print(list(takewhile(gtx, data)))
