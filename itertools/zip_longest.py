from itertools import zip_longest

seq_short = [1, 2, 3]
seq_long = [1, 2, 3, 4, 5, 6]
data = [100, 200, 300, 400]

print(list(zip_longest(seq_short, data)))

print(list(zip_longest(seq_long, data)))
