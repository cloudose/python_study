from itertools import repeat

# repeat
c = repeat(10)
for _ in range(10):
    print(next(c))

# repeat 3 times
c2 = repeat(99, 3)
for i in c2:
    print(i)

print(list(map(pow, range(10), repeat(2))))
print(list(map(pow, range(10), repeat(3))))
