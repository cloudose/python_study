from itertools import compress, chain

letters = ['a', 'b', 'c', 'd']
numbers = [0, 1, 2]
names = ['Corey, Nicole']


selectors = [False, True, False, True]


def sel_gen():
    for x in selectors:
        yield x


print(list(compress(letters, selectors)))

print(list(filter(lambda x: x[1], zip(letters, sel_gen()))))
