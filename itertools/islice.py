''' slice an iterator and returns a new iterator'''
from itertools import islice

print(type(islice(range(10), 1)))

print(list(islice(range(10), 2, 5)))  # 2-5
print(list(islice(range(10), 5)))   # 0-5
print(list(islice(range(10), 2, 10, 3)))  # 2-10, step 2


with open('itertools/sample.txt', 'r') as fh:
    print(list(islice(fh, 3)))
