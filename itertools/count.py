import itertools

counter = itertools.count()

for _ in range(100):
    print(next(counter), end=' ')
print()

data = [200, 300, 400, 500]
for i in zip(itertools.count(), data):
    print(i)
for i in zip(itertools.count(), data):
    print(i)


counter2 = itertools.count(start=5, step=-2.5)
print(next(counter2))
print(next(counter2))
print(next(counter2))
