from itertools import combinations, permutations, product, combinations_with_replacement


letters = ['a', 'b', 'c', 'd']
numbers = [0, 1, 2]
names = ['Corey, Nicole']

# can't repeat: race result
print(list(permutations(letters, 4)))

# can't repeat: team building
print(list(combinations(letters, 2)))

# can repeat. password brutal force!
print('product', list(product(numbers, repeat=3)))
print('c_rp', list(combinations_with_replacement(numbers, 3)))

print('permute', list(permutations(numbers, 3)))
